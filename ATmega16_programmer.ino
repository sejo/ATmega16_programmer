/*
    Sejo's ATmega16 Serial Programmer
    Copyright (C) 2012 Jose M. Vega-Cebrian jmvegacebrian [at] gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************************************

This is a programmer for the ATmega16 microcontroller using its SPI interface.

HOW TO USE
------------------------------------
1) Compile this file and upload it to your Arduino
2) Make the connections between the Arduino board and the ATmega16 as described below
3) Open a terminal (e.g. minicom) and configure the connection as described below
4) Reset the Arduino and follow the instructions in the terminal


Arduino - ATmega16 Connections
------------------------------------
In order to work, some connections have to be made between the Arduino board and the ATmega16:
  Pins used:
	-------------------------------------------------------
  	| Arduino Uno	| ATmega16		| Function
	-------------------------------------------------------
  	| 13		| PB7 (8)		| SCK (SPI)
  	| 12		| PB6 (7)		| MISO (SPI)
  	| 11		| PB5 (6)		| MOSI (SPI)
  	| 10		| RESET	(9)		| SS / RESET
	| +5V		| VCC, AVCC (10,30)	| Power
	| GND		| GND (11)		| Ground
	-------------------------------------------------------
Those are the only connections needed if an internal clock is used in the ATmega16.
In other case, the used clock must be present and connected.

This program assumes the internal 1MHz oscillator is used.
If instead the 8MHz one is configured, the SPI Clock Divider can be SPI_CLOCK_DIV16 and probably
the UART baud rate can be increased.


Computer - Arduino Communication
------------------------------------
The communication with a computer happens through an UART with the following parameters:

	Baud rate: 9600 baud/s
	Number of bits: 8
	Parity bits: none
	Stop bits: 2*

	9600 8N2

* I had some problems using only 1 stop bit, however it might work.
I don't recommend the Arduino Serial Monitor because it's limited and can't easily handle files.


Program flow
------------------------------------
* Asks the user to ensure the connections are made
* Resets the ATmega16 and tries to enable the Serial Programming
* Reads signature bytes to find out if device is indeed an ATmega16 (must be 0x1E 0x94, 0x03)
* Asks the user to confirm chip erasing
* Asks for the HEX file and writes the code into memory while parsing it


HEX files
------------------------------------
This program will work only if everything in the code is word-aligned, i.e. in 16-bits blocks.
That's how usually a compiled / assembled program will be.
Care must be taken when using directives to write bytes directly into program memory, as theoretically
they could unalign the blocks. 
(I haven't programmed yet in this microcontroller and I don't know how its assembler or compiler work)

This program can parse the following record types in the HEX file:
	* Data record
	* End of file record
As far as I know, that's enough for what a compiler or assembler will usually do.

The line breaks supported are:
	* '\n' (UNIX, GNU/Linux)
	* "\r\n" (Windows)
	

Future work
------------------------------------
This is an invitation to improve the program. 
Some ideas:
	* Implement every Serial Programming instruction from the set (e.g. Fuses and EEPROM)
	* Develop a better interface (Read memory blocks, just erase the chip, write EEPROM...)
	* Formally parse HEX files (don't ignore checksum, understand more record types)
	* Better error handling
	* Diagnose causes of hardware errors
	* Be able to program more microcontrollers :D

Please check that there are some functions that I defined and used but in this final version are
not implemented. Take the time to read the code so you don't end up repeating what I've already done.

*/

#define PIN_RESET	10
#define PIN_MOSI	11
#define PIN_MISO	12
#define PIN_SCK		13

#define PAGE_SIZE	64 // words
#define NUM_PAGES	128

#define PROG_EN_TIMEOUT	10	// Number of times the programming enable routine is attempted before timeout

#include <SPI.h>

void setup(){

	unsigned int i;
	unsigned char success;
	unsigned char signature[3];
        unsigned short _word;


	// Serial setup
	Serial.begin(9600);
	Serial.println();
	Serial.println();
	Serial.println();
	Serial.println();
	Serial.println("Welcome to Sejo's ATmega16 Serial Programmer!"); 
	Serial.println();
	Serial.println("Sejo's ATmega16 Serial Programmer Copyright (C) 2012 Jose M. Vega-Cebrian");
	Serial.println("This program comes with ABSOLUTELY NO WARRANTY;");
	Serial.println("This is free software, and you are welcome to redistribute it");
	Serial.println("under certain conditions.");
	Serial.println();
	Serial.println();
	Serial.println();
	Serial.println("MAKE SURE THE ATmega16 IS CONNECTED. PRESS ANY KEY TO CONTINUE");
	wait_for_any_key();
	Serial.println();
	Serial.println();



	// SPI setup
	SPI.setClockDivider(SPI_CLOCK_DIV128); //Assuming ATmega16 1MHz internal clock
	SPI.setBitOrder(MSBFIRST);
	SPI.setDataMode(SPI_MODE0); // CPOL=0, CPHA =0
	pinMode(PIN_RESET,OUTPUT);
	digitalWrite(PIN_RESET,LOW);
	SPI.begin();

	// Reset pulse
        delay(20);
	digitalWrite(PIN_RESET,HIGH);
	delay(20);
	digitalWrite(PIN_RESET,LOW);
	delay(20);


	// Programming Enable
	Serial.println("Enabling memory programming...");
	i = 0;
	do{
		success = ins_programming_enable();
		if(!success){
			if(++i == PROG_EN_TIMEOUT){
				// Timeout!
				Serial.println("Couldn't enable memory programming! :(");
				Serial.println("Check your connections and reset Arduino when ready");
				while(1);
			}
			else{
				// Reset pulse
				digitalWrite(PIN_RESET,HIGH);
				delay(20);	
				digitalWrite(PIN_RESET,LOW);
				delay(20);
			}

		}
	} while(!success);
	Serial.println("Memory programming enabled!");
	wait_until_chip_is_ready();

	// Reading and verifying signature
	Serial.println("Reading signature...");
	for(i=0; i<3; i++){
		signature[i] = ins_read_signature_byte(i);
		Serial.print(signature[i],HEX);
		Serial.print(" ");
		wait_until_chip_is_ready();
	}
	Serial.println();
	if(signature[0]==0x1E && signature[1]==0x94 && signature[2]==0x03){
		Serial.println("ATmega16 detected!");
	}
	else{
		Serial.println("ATmega16 was not detected :(");
		Serial.println("Reset Arduino when ready to try again");
		while(1);
	}
	Serial.println();
	Serial.println();
	/* Read some memory before erasing*/
	//read_program_memory(0,0x8F);
	

	// Chip erasing
	Serial.println("Chip needs to be erased. Press any key to continue");
	wait_for_any_key();
	Serial.println();
	Serial.println();
	Serial.println("Erasing chip...");
	ins_chip_erase();
	wait_until_chip_is_ready();
	Serial.println("Chip erased!");
	delay(10);
	Serial.println();
	Serial.println();


	// Receive file
	process_hex_file();

	// Finish
	Serial.println();
	Serial.println();
	Serial.println("Now you can disconnect the ATmega16");
	Serial.println("Reset the Arduino to start again");

	/* Read some memory after programming*/
	//read_program_memory(0,0x8F);
}

void process_hex_file(){
	unsigned short page[PAGE_SIZE];
	unsigned short address;
	unsigned char page_address = 0xFF;
	unsigned char last_page_address_written = 0xFF;
	unsigned char prev_page_address;
	unsigned short _word;

	unsigned char byte_count;
	unsigned char record_type;
	unsigned char checksum;
        unsigned int i;
        unsigned char success;
	reset_page(page);
	success = 0;
	Serial.println("Send HEX file to program:");
	while(Serial.available()<1);
	Serial.println("Programming...");
	do{
		if(Serial.available()>=9){ // Waits until "header" is received
                        if(Serial.read()!=':'){            // First character must be ':'
				Serial.print(address,HEX);
				call_error_parsing();
			}

			// Read the different fields from the "header"
			byte_count = ascii2num(Serial.read())<<4;
			byte_count |= ascii2num(Serial.read());
			prev_page_address = page_address;
			address = ascii2num(Serial.read())<<12;
			address |= ascii2num(Serial.read())<<8;
			address |= ascii2num(Serial.read())<<4;
			address |= ascii2num(Serial.read());
			address = address / 2; // Because HEX address is byte address and we need word address
			record_type = ascii2num(Serial.read())<<4;
			record_type |= ascii2num(Serial.read());
			page_address = address >> 6;

			if(((prev_page_address != page_address) || record_type==1) && (last_page_address_written!=prev_page_address)){ // Checks if prev page has been written
				write_page(prev_page_address,page);
				reset_page(page);
			}
			/*
			Debug. Don't use if sending complete file from a terminal because programming will fail due to lack of processing time
			Serial.print("Byte count: ");
			Serial.println(byte_count,HEX);
			Serial.print("Address: ");
                        Serial.println(address,HEX);
			Serial.print("Page address: ");
			Serial.println(page_address,HEX);
			Serial.print("Record type: ");
			Serial.println(record_type,HEX);
                        */
			
			if(record_type==1){ // End of File

				while(Serial.available()<2);
				Serial.read();
				Serial.read();
				success = 1;
			}
			else if(record_type==0){ // Data
				for(i=0; i<byte_count/2; i++){
					_word = 0;
					while(Serial.available()<4); // Wait to receive data
					_word |= ascii2num(Serial.read())<<12;
					_word |= ascii2num(Serial.read())<<8;
					_word |= ascii2num(Serial.read())<<4;     
					_word |= ascii2num( Serial.read());
					page[(address&0x3F)+i] = _word;
				}
				if(((address + byte_count/2) & 0x3F) == 0){ // If last bytes in the page
					// Page needs to be written                             
					write_page(page_address,page);
					reset_page(page);
					last_page_address_written = page_address;
				}

				// Read checksum
				while(Serial.available()<3);
				checksum = ascii2num(Serial.read())<<4;
				checksum |= ascii2num(Serial.read());

				// Read End of line
				if(Serial.read()== '\r'){
					while(Serial.available()<1);
					Serial.read(); // \n
				}
			}
			else{
				call_error_parsing();
			}
		}

	} while (!success);
	/* Debug last written page
	for(i=0;i<0x40;i++){
		Serial.println(page[i],HEX);
	}
	*/
	Serial.println("SUCCESSFULLY PROGRAMMED! :)");
}

void loop(){
	// bla
	
}


// *************************************************************************************
// SPI instruction set
// *************************************************************************************
// See ATmega16 datasheet.
//

unsigned char ins_programming_enable(){
	unsigned char echo = 0;
	SPI.transfer(0xAC);
	SPI.transfer(0x53);
	echo = SPI.transfer(0x00);
	SPI.transfer(0x00);

	return (echo==0x53) ? 1 : 0;
}

unsigned char ins_poll_isBusy(){
	unsigned char byte = 0;
	SPI.transfer(0xF0);
	SPI.transfer(0x00);
	SPI.transfer(0x00);
	byte = SPI.transfer(0x00);

	return byte; // 1: busy, 0: ready
}

void ins_chip_erase(){
	SPI.transfer(0xAC);
	SPI.transfer(0x80);
	SPI.transfer(0x00);
	SPI.transfer(0x00);
}

unsigned char ins_read_signature_byte(unsigned char address){
	unsigned char byte = 0;
	SPI.transfer(0x30);
	SPI.transfer(0x00);
	SPI.transfer(address);
	byte = SPI.transfer(0x00);
	return byte;
}

void ins_load_program_memory_page_byte(unsigned short address, unsigned char byte, unsigned char is_msb){ // address: absolute address, byte: byte to load, is_msb: 1 MSB, 0 LSB
	if(is_msb){
		SPI.transfer(0x48);
	}else{
		SPI.transfer(0x40);
	}
	SPI.transfer(0xFF&(address>>8));
	SPI.transfer(0xFF&address);
	SPI.transfer(byte);
}

void ins_write_program_memory_page(unsigned char page_address){
	SPI.transfer(0x4C);
	SPI.transfer(0x1F&(page_address>>2));
	SPI.transfer((page_address&0x03)<<6);
	SPI.transfer(0x00);
}

unsigned char ins_read_program_memory_byte(unsigned short address, unsigned char is_msb){
	unsigned char byte = 0;
	if(is_msb){
		SPI.transfer(0x28);
	}
	else{
		SPI.transfer(0x20);
	}
	SPI.transfer(address>>8);
	SPI.transfer(address&0xFF);
	byte = SPI.transfer(0x00);
	return byte;
}


// *************************************************************************************
// Memory Programming Higher level functions
// *************************************************************************************
//
// Wait until chip is ready.
// Polls until ready
void wait_until_chip_is_ready(){
	while(ins_poll_isBusy()==1);
}

// Load a word into absolute address
// address: absolute address, 5-0 is offset within page, 14-6 is page number
// word: word to load
void load_program_memory_page_word(unsigned short address, unsigned short _word){
	ins_load_program_memory_page_byte(address, 0xFF&_word, 0);
	wait_until_chip_is_ready();
	ins_load_program_memory_page_byte(address, 0xFF&(_word>>8), 1);
	wait_until_chip_is_ready();
}

// Write a page of program memory
// page_address: from 0 to NUM_PAGES-1
// page: should be an array of PAGE_SIZE words
void write_page(unsigned char page_address, unsigned short * page){
	unsigned short address = page_address<<6;
	unsigned char offset = 0;
	for(offset=0; offset<PAGE_SIZE; offset++, address++){
		load_program_memory_page_word(address, page[offset]);
	}
	ins_write_program_memory_page(page_address);
	wait_until_chip_is_ready();
}

// Resets a page array
// page: an array of PAGE_SIZE words
void reset_page(unsigned short * page){
	unsigned char i;
	for(i=0; i<PAGE_SIZE; i++){
		page[i] = 0xFFFF;
	}
}

// *************************************************************************************
// Memory Reading Higher level functions
// *************************************************************************************
// Read a word from absolute address
// address: absolute address
// Returns the read word
unsigned short read_program_word(unsigned short address){
	unsigned short _word = 0;
	wait_until_chip_is_ready();
	_word = ins_read_program_memory_byte(address,0);
	wait_until_chip_is_ready();
	_word |= ins_read_program_memory_byte(address,1)<<8;
	wait_until_chip_is_ready();
	return _word;
}

// Read a section of the program memory and print it
// start_address: absolute address from where to start
// end_address: last address that will be read
void read_program_memory(unsigned short start_address, unsigned short end_address){
	unsigned int i;
	unsigned short _word;
	for(i=start_address; i<=end_address; i++){
		if(!(i%0x10)){
			Serial.print(i,HEX);
			Serial.print("\t");
		}
        	_word = read_program_word(i);
		Serial.print(_word,HEX);
		if((i&0xF) == 0xF){
			Serial.println();
		}
                else{
			Serial.print("\t");
		}
	}
}


// *************************************************************************************
// HEX parsing
// *************************************************************************************

// Converts an hexadecimal character to number
unsigned char ascii2num(unsigned char c){
	return (c<0x3A) ? (c-0x30) : (c-0x37);
}

void call_error_parsing(){
	Serial.println("Error reading your file :(");
	Serial.println("Reset Arduino to try again");
	while(1);
}


// *************************************************************************************
// Misc
// *************************************************************************************

void wait_for_any_key(){
	while(Serial.available()<1);
	Serial.read();
}



