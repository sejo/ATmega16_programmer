# ATmega16_programmer

    Sejo's ATmega16 Serial Programmer
    Copyright (C) 2012 Jose M. Vega-Cebrian jmvegacebrian [at] gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*************************************************************************************************************

This is an Arduino programmer for the ATmega16 microcontroller using its SPI interface.

HOW TO USE
------------------------------------
1) Compile this file and upload it to your Arduino
2) Make the connections between the Arduino board and the ATmega16 as described below
3) Open a terminal (e.g. minicom) and configure the connection as described below
4) Reset the Arduino and follow the instructions in the terminal


Arduino - ATmega16 Connections
------------------------------------
In order to work, some connections have to be made between the Arduino board and the ATmega16:
  Pins used:
  
	-------------------------------------------------------
  	| Arduino Uno	| ATmega16		      | Function
	-------------------------------------------------------
  	| 13		      | PB7 (8)		        | SCK (SPI)
  	| 12		      | PB6 (7)		        | MISO (SPI)
  	| 11		      | PB5 (6)		        | MOSI (SPI)
  	| 10		      | RESET	(9)		    | SS / RESET
    | +5V		      | VCC, AVCC (10,30)	| Power
	 | GND		      | GND (11)		      | Ground
	-------------------------------------------------------
	
Those are the only connections needed if an internal clock is used in the ATmega16.
In other case, the used clock must be present and connected.

This program assumes the internal 1MHz oscillator is used.
If instead the 8MHz one is configured, the SPI Clock Divider can be SPI_CLOCK_DIV16 and probably
the UART baud rate can be increased.


Computer - Arduino Communication
------------------------------------
The communication with a computer happens through an UART with the following parameters:

	Baud rate: 9600 baud/s
	Number of bits: 8
	Parity bits: none
	Stop bits: 2*

	9600 8N2

* I had some problems using only 1 stop bit, however it might work.
I don't recommend the Arduino Serial Monitor because it's limited and can't easily handle files.


Program flow
------------------------------------
* Asks the user to ensure the connections are made
* Resets the ATmega16 and tries to enable the Serial Programming
* Reads signature bytes to find out if device is indeed an ATmega16 (must be 0x1E 0x94, 0x03)
* Asks the user to confirm chip erasing
* Asks for the HEX file and writes the code into memory while parsing it


HEX files
------------------------------------
This program will work only if everything in the code is word-aligned, i.e. in 16-bits blocks.
That's how usually a compiled / assembled program will be.
Care must be taken when using directives to write bytes directly into program memory, as theoretically
they could unalign the blocks. 
(I haven't programmed yet in this microcontroller and I don't know how its assembler or compiler work)

This program can parse the following record types in the HEX file:
* Data record
* End of file record
As far as I know, that's enough for what a compiler or assembler will usually do.

The line breaks supported are:
* '\n' (UNIX, GNU/Linux)
* "\r\n" (Windows)
	

Future work
------------------------------------
This is an invitation to improve the program. 
Some ideas:
* Implement every Serial Programming instruction from the set (e.g. Fuses and EEPROM)
* Develop a better interface (Read memory blocks, just erase the chip, write EEPROM...)
* Formally parse HEX files (don't ignore checksum, understand more record types)
* Better error handling
* Diagnose causes of hardware errors
* Be able to program more microcontrollers :D

Please check that there are some functions that I defined and used but in this final version are
not implemented. Take the time to read the code so you don't end up repeating what I've already done.
